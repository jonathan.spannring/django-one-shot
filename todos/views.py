from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.
def todo_list_view(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list": todo_list
    }
    return render(request, "todos/list.html", context)

def todo_detail(request, id):
    list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_detail": list,
    }
    return render(request, "todos/detail.html", context)

def create_todo(request):
    if request.method == 'POST':
        form = TodoListForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect('todo_detail', id=item.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, 'todos/create_list.html', context)

def edit_todo(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=post)
        if form.is_valid():
            item = form.save()
            return redirect('todo_detail', post.id)
    else:
        form = TodoListForm(instance=post)
    context = {
        "form": form,
        "todo_object": post,
    }
    return render(request, "todos/edit_list.html", context)

def delete_todo(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")

def create_item(request):
    if request.method == 'POST':
            form = TodoItemForm(request.POST)
            if form.is_valid():
                item = form.save()
                return redirect('todo_detail', id=item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, 'todos/create_item.html', context)

def edit_item(request, id):
    post = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=post)
        if form.is_valid():
            item = form.save()
            return redirect('todo_detail', id=item.list.id)
    else:
        form = TodoItemForm(instance=post)
    context = {
        "form": form,
        "todo_object": post.list.id,
    }
    return render(request, "todos/edit_item.html", context)
